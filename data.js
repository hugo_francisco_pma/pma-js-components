var ExampleData =
	{
		config: {
			apiKey: "AIzaSyB7I7as8mIhtG8-a7nrkqDENMhw-X0Vitc",
			mapZoom: 3,
			mapCenter: { lat: 36.500688, lng: -16.104415 },
			unitSystem: 0
		},
		favourites: [
			{
				title: "Ponta Delgada",
				subtitle: "Nonagon -> Ponta Delgada",
				origin: "37.749767, -25.580102",
				destination: "37.742496, -25.680475"
			},
			{
				title: "Ribeira Grande",
				subtitle: "Nonagon -> Ribeira Grande",
				origin: "37.820970, -25.517850",
				destination: "37.742496, -25.680470"
			},
		],
		route: {
            originText: undefined,
            originLat: undefined,
            originLong: undefined,
            destinationText: undefined,
            destinationLat: undefined,
            destinationlong: undefined,
            distance: undefined,
            overrideDistance: undefined,
            overrideReason: undefined,
            path: undefined,
            distance: undefined,
            override: false,
            overrideDistance: undefined,
			overrideReason: undefined,
			odometerStart: undefined,
			odometerEnd: undefined
        }
	};