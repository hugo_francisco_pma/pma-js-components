function PMARateModal(modalID) {
    this.modal = $('#' + modalID);

    //
    // initialize our components elements
    //
    this.odoDiv = $('#odoDiv');
    this.gmapDiv = $('#gmapDiv');
    this.modalTitle = $('#modalTitle');
}


PMARateModal.prototype.init = function (options) {
    //
    // initializing our fields
    //
    this.ammountFieldId = options.ammountFieldId;
    this.expCode = options.expCode;
    this.date = options.date;
    this.finalId = options.finalId;
    this.tempId = options.tempId;

    //var url = thisrateId ? "" : "";
    // fetch data on server
    //TODO: generate the correct url
    $.get("http://192.168.1.49/PSa_2/Server/Psa_2.DLL?FTYPE=L&FORMID=PMAAPIEXPRATELOAD&KEXPENSEDETAIL_ID=1028&USERID=ADMIN&UPSW=ADMIN")
        .done(this.showModal.bind(this))
        .fail(this.showError.bind(this));
}

PMARateModal.prototype.showModal = function (data) {
    //
    // parse our json
    //
    var dataObj = JSON.parse(data);

    //
    // initialize our modal type
    //
    this.shouldUseGoogleMaps = false;
    //dataObj.config.shouldUseGoogleMaps;

    //
    // remove last postion of our favourite's array because it is a dummy item
    //
    dataObj.favourites.splice(-1, 1);

    //
    // initialize our data object to be passed onto our components
    //
    var data = {
        config: dataObj.config,
        favourites: dataObj.favourites,
        route: dataObj.expRate
    }

    // prepare modal
    this.odoDiv.hide();
    this.gmapDiv.hide();

    //
    // removing every events
    //
    this.modal.off();

    var modalTitleText = 'Distance Calculator';

    var action;
    if (this.shouldUseGoogleMaps) {
        modalTitleText = "Google Maps Calculator";
        action = this.initPMAMap.bind(this);
        this.gmapDiv.show();
    } else {
        action = this.initPMAOdometer.bind(this);
        this.odoDiv.show();
    }

    this.modal.on("shown.bs.modal", function () {
        action(data);
    });

    this.modal.on("hidden.bs.modal", function () {
        this.currentControl.resetInterface();
    }.bind(this));

    //
    // set modal title
    //
    this.modalTitle.html(modalTitleText);

    //
    // openModal
    //
    this.modal.modal('show');
}

PMARateModal.prototype.showError = function (error) {
    alert(error);
}

PMARateModal.prototype.initPMAMap = function (data) {
    this.currentControl = new PMAMap(
        {
            routeSettings: {
                originFieldId: "origin",
                destinationFieldId: "destination",
                distanceFieldId: "distance",
                overrideCheckBoxDivId: "overrideCheckBoxDiv",
                overrideContentWrapperId: "overrideContentWrapper",
                overrideDistanceFieldId: "overrideDistance",
                overrideReasonId: "overrideReason",
                mapPlaceholderId: "map",
                distanceLabelId: "distanceLabel",
                buttonSaveId: "buttonSave",
                distanceGroupId: "distanceGroup",
                mapGroupId: "mapGroup"
            },
            favouriteSettings: {
                comboBoxId: "comboboxHistory"
            },
            data: data
        });
}

PMARateModal.prototype.initPMAOdometer = function (data) {
    this.currentControl = new PMAOdometer(
        {
            odometerStartId: "odometerStart",
            odometerEndId: "odometerEnd",
            odometerDistanceId: "odometerDistance",
            odometerCheckboxId: "odometerCheckbox",
            labelRateId: "labelRate",
            odometerDistanceLabelId: "odometerDistanceLabel",
            alertDivId: "alertDiv",
            rate: 0.5, //data.expCode.rate
            unitSystem: 1 //data.unitSystem
        });
}

PMARateModal.prototype.calculate = function () {
    if (this.currentControl.validate()) {
        this.currentControl.calculate();
    }
}

PMARateModal.prototype.save = function () {
    //
    // grabing info from our currentControl
    //
    if (this.currentControl.validate(PMAMapUtils.validationTypes.SUBMIT)) {
        var exportedData = this.currentControl.export();

        var rate = {
            odometerStart: 0,
            odometerEnd: 0,
            odometerDistance: 0
        }

        $.extend(rate, exportedData);

        $.get("http://192.168.1.26/A4ExpenseSQL/Server/A4ExpenseSQL.dll?FTYPE=L&FORMID=A4APIEDEXPENSECODELINELOAD&USERID=ADMIN&UPSW=ADMIN&KID=2031")
            .done(this.dismissModal.bind(this))
            .fail(this.showError.bind(this));

    }
}

PMARateModal.prototype.dismissModal = function () {
    this.modal.modal('hide');
}

PMARateModal.prototype.odometerCheckboxChanged = function () {
    this.currentControl.odometerCheckboxChanged();
}

PMARateModal.prototype.togglePmaMapOverride = function () {
    this.currentControl.toggleOverrideContent();
}