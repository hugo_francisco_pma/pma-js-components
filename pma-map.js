//
// set of utilities to be used within PMAMap oject
//
var PMAMapUtils = {
    validationTypes:
    {
        CALCULATE_DISTANCE: "CALCULATE_DISTANCE",
        SUBMIT: "SUBMIT"
    },
    constants: {
        ORIGIN_MANDATORY: "Origin is mandatory",
        DESTINATION_MANDATORY: "Destination is mandatory",
        ROUTE_ERROR: "Oops, there's no route between these two locations",

        UNITS: [
            "Km",
            "Mi"
        ],
        OVERRIDE_REASON_MANDATORY: "Override reason is mandatory",
        OVERRRIDE_DISTANCE_MANDATORY: "Override distance is mandatory ",
    },
    converters: [
        //
        // use case: converters[unitSystem](valueInMeters);
        //

        //
        // position 0 of the array (meters to km)
        //
        function (m) {
            return m / 1000.0;
        },

        //
        // position 1 of the array (meters to miles)
        //
        function (m) {
            return m * 0.00062137;
        }
    ],
    defaultData: {
        config: {
            apiKey: "",
            mapZoom: 3,
            mapCenter: { lat: 36.500688, lng: -16.104415 },
            unitSystem: 1, //google.maps.UnitSystem.METRIC = 0 AND google.maps.UnitSystem.IMPERIAL = 1,
            shouldHideMap: false
        },
        favourites: [

        ],
        route: {
            originText: '',
            originLat: 0,
            originLng: 0,
            destinationText: '',
            destinationLat: 0,
            destinationLng: 0,
            distance: '',
            override: false,
            overrideDistance: 0,
            overrideReason: '',
            path: ''
        }
    }
}

//
// PMAMap object
//
function PMAMap(options) {
    //
    // we need to get our google maps script
    // 
    var googleMapsScriptURL = `https://maps.googleapis.com/maps/api/js?key=${options.data.config.apiKey}&libraries=places,geometry&sensor=true`;

    var scriptExists = $("script[src*='" + googleMapsScriptURL + "']");

    //
    // check if we loaded google maps already?
    //
    if (typeof google === "undefined" || typeof google.maps === "undefined") {
        //
        // we haven't loaded google maps api just yet, lets load it
        //
        $.getScript(googleMapsScriptURL).then(function () {
            this.init(options);
        }.bind(this));
    }
    else {
        //
        // we have loaded google maps already, just init
        //
        this.init(options);
    }
}

PMAMap.prototype.init = function (options) {
    this.initGoogleMapPrototypes();
    this.defaults(options);

    //
    // setting placeholder variables for our ui elements
    //
    this.mapPlaceholder = $("#" + options.routeSettings.mapPlaceholderId);
    this.originField = $("#" + options.routeSettings.originFieldId);
    this.destinationField = $("#" + options.routeSettings.destinationFieldId);
    this.distanceLabel = $("#" + options.routeSettings.distanceLabelId);
    this.distanceField = $("#" + options.routeSettings.distanceFieldId);
    this.overrideContentWrapper = $("#" + options.routeSettings.overrideContentWrapperId);
    this.overrideDistance = $("#" + options.routeSettings.overrideDistanceFieldId);
    this.overrideReason = $("#" + options.routeSettings.overrideReasonId);
    this.distanceGroup = $("#" + options.routeSettings.distanceGroupId);
    this.mapGroup = $("#" + options.routeSettings.mapGroupId);
    //
    // init our map
    //
    this.map = new google.maps.Map(document.getElementById(options.routeSettings.mapPlaceholderId), {
        zoom: this.mapZoom,
        center: this.mapCenter
    });

    //
    // init our favourites combobox
    //
    this.comboBox = $("#" + options.favouriteSettings.comboBoxId).kendoComboBox({
        template: $("<div><h4>#:data.title#</h4><span>#:data.subtitle#</span></div>").html(),
        dataTextField: "title",
        dataSource: this.data.favourites,
        //
        // binding our current context so we don't lose context
        //
        change: this.choseElementFavourite.bind(this),
        noDataTemplate: 'No Data!'
    });

    //
    // create our origin and destination autocompletes
    //
    this.originAutoComplete = new google.maps.places.Autocomplete(this.originField.get(0));
    this.destinationAutoComplete = new google.maps.places.Autocomplete(this.destinationField.get(0));

    //
    // clear interfance
    //
    this.clearAll();

    //
    // fill our data
    //
    this.fill(this.data);

    //
    // appending our unit to the distance label so the user understands what unit system is in use
    //

    //
    // set groups visibility
    //
    this.setOverrideWrapperVisibility();
    this.setDistanceGroupVisibility();
    this.hideMap();
}

PMAMap.prototype.defaults = function (options) {
    //
    // $.extend received options with the default ones
    //
    var _data = $.extend(true, {}, PMAMapUtils.defaultData, options.data);

    //
    // set our config
    //
    this.mapZoom = _data.config.mapZoom;
    this.mapCenter = _data.config.mapCenter;
    this.directionsService = new google.maps.DirectionsService();
    this.unitSystem = _data.config.unitSystem;

    //
    // set our internal data object
    //
    this.data = _data;
}

PMAMap.prototype.clearAll = function () {
    //
    // clear all fields 
    //
    this.originField.val("");
    this.destinationField.val("");
    this.distanceField.val("");
    this.overrideDistance.val("");
    this.overrideReason.val("");
}

PMAMap.prototype.clearDistanceFields = function () {
    var route = this.data.route;

    route.distance = undefined;
    this.distanceField.val(undefined);

    route.overrideDistance = undefined;
    this.overrideDistance.val(undefined);
}

PMAMap.prototype.fill = function (data) {

    this.originField.val(data.route.originText);
    this.destinationField.val(data.route.destinationText);
    this.distanceField.val(data.route.distance);
    this.overrideDistance.val(data.route.overrideDistance);
    this.overrideReason.val(data.route.overrideReason);

    //
    // draw the path, should there be one
    //
    if (data.route.path) {
        data.route.path = window.atob(data.route.path);
        this.drawPolyline(data.route.path, true);
    }

    //
    // fill in our unit system in it's label
    //
    this.distanceLabel.html("Distance (" + PMAMapUtils.constants.UNITS[this.unitSystem] + ")");
}

PMAMap.prototype.setDistanceGroupVisibility = function () {
    if (this.data.route.distance) {
        this.distanceGroup.show();
    }
    else {
        this.distanceGroup.hide();
    }
}

PMAMap.prototype.setOverrideWrapperVisibility = function () {
    if (this.data.route.override) {
        this.overrideContentWrapper.show();
    }
    else {
        this.overrideContentWrapper.hide();
    }
}

PMAMap.prototype.drawPolyline = function (path, isFromServer) {
    //
    // clear any previously drawn route
    //
    if (this.route) {
        this.route.setMap(null);
    }

    //
    // get our path
    //
    var pathPoints = google.maps.geometry.encoding.decodePath(path);

    //
    // should we simplify?
    //
    if (!isFromServer) {
        pathPoints = simplifyPath(pathPoints, 165);
    }

    //
    // create our polyline
    //
    this.route = new google.maps.Polyline({
        path: pathPoints,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: this.map
    });


    //
    // should we fit bounds we're going to zoom in our path
    //
    if (isFromServer) {
        this.map.fitBounds(this.route.getBounds());
    }
}

PMAMap.prototype.choseElementFavourite = function () {
    var selectedItem = this.comboBox.data("kendoComboBox").dataItem();
    this.originField.val(selectedItem.origin);
    this.destinationField.val(selectedItem.destination);
    this.clearDistanceFields();
}

PMAMap.prototype.validate = function (type) {
    //
    // validate origin and destination text
    //

    var errors = [];
    var isValid = true;

    if (!this.originField.val()) {
        errors.push(PMAMapUtils.constants.ORIGIN_MANDATORY);
    }
    if (!this.destinationField.val()) {
        errors.push(PMAMapUtils.constants.DESTINATION_MANDATORY);
    }
    if (type === PMAMapUtils.validationTypes.SUBMIT) {

        var _data = this.data.route;
        //
        // validate the rest of the fields
        //
        if (!_data.distance) {
            errors.push("Please calculate distance");
        }

        // get override distance and reason from the ui
        _data.overrideDistance = this.overrideDistance.val();
        _data.overrideReason = this.overrideReason.val();

        if (_data.override) {
            if (!_data.overrideDistance) {
                errors.push(PMAMapUtils.constants.OVERRRIDE_DISTANCE_MANDATORY);
            }
            if (!_data.overrideReason) {
                errors.push(PMAMapUtils.constants.OVERRIDE_REASON_MANDATORY);
            }
        }
    }
    //
    // not valid? set our errors
    //
    isValid = errors.length == 0;
    if (!isValid) {
        isValid = false;

        var stringErrors = "";
        errors.forEach((element) => {
            stringErrors += element;
            stringErrors += "\n";
        });

        alert(stringErrors);
    }

    return isValid;
}

PMAMap.prototype.calculate = function () {
    this.resetInterface();
    //
    // do we have a directions object? if so, we need to clear it from the map
    //
    if (this.directions) {
        this.directions.setMap(null);
        this.directions = undefined;
    }

    //
    // creating an auxiliar to access our route
    //
    var route = this.data.route;

    //
    // setting our new route origin and destination text values
    //
    route.originText = this.originField.val();
    route.destinationText = this.destinationField.val();

    //
    // prepare our route request
    //
    var request = {
        origin: route.originText,
        destination: route.destinationText,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        unitSystem: this.unitSystem,
        provideRouteAlternatives: false
    };

    //
    // setup an auxiliar variable to keep track of our context
    //
    this.directionsService.route(request, this.responseCallback.bind(this));
}

PMAMap.prototype.responseCallback = function (response, status) {
    //
    // do we have a route?
    //
    if (status == google.maps.DirectionsStatus.OK) {

        this.successCallback(response);


    } else {
        //
        // prepare our error for delivery
        //
        this.errorCallback(PMAMapUtils.constants.ROUTE_ERROR);
    }
}

PMAMap.prototype.successCallback = function (response) {
    var gmRoute = response.routes[0];
    var leg = gmRoute.legs[0];
    //
    // initialize our directons object and draw it on the map
    //
    this.directions = new google.maps.DirectionsRenderer();
    this.directions.setMap(this.map);
    this.directions.setDirections(response);

    //
    // getting our shortest route
    //
    var route = this.data.route;
    route.path = gmRoute.overview_polyline;

    //
    // set new values to our data object
    //
    route.originLat = leg.start_location.lat().toFixed(6);
    route.originLng = leg.start_location.lng().toFixed(6);

    route.destinationLat = leg.end_location.lat().toFixed(6);
    route.destinationLng = leg.end_location.lng().toFixed(6);

    route.distance = PMAMapUtils.converters[this.unitSystem](leg.distance.value).toFixed(2);
    //
    // change the values on the UI
    //
    this.drawPolyline(route.path, false);

    this.distanceField.val(route.distance);
    this.overrideDistance.val(route.distance);
    this.setDistanceGroupVisibility();
}

PMAMap.prototype.resetInterface = function () {
    this.distanceGroup.hide();
    this.overrideContentWrapper.hide();
    this.data.route.override = false;
}

PMAMap.prototype.errorCallback = function (error) {
    alert(error);
}

PMAMap.prototype.toggleOverrideContent = function () {
    this.overrideContentWrapper.toggle();
    var _route = this.data.route;

    //
    // set our local variables
    //
    _route.override = !_route.override;
    _route.overrideReason = _route.override ? _route.overrideReason : undefined;
    _route.overrideDistance = _route.distance;


    //
    // set the values on the UI
    //
    this.overrideReason.val(_route.overrideReason);
    this.overrideDistance.val(_route.overrideDistance);
}

PMAMap.prototype.hideMap = function () {
    if (this.data.config.shouldHideMap) {
        this.mapGroup.hide();
    }
}

PMAMap.prototype.showMap = function () {
    if (this.data.config.shouldHideMap) {
        this.mapGroup.show();
        google.maps.event.trigger(this.map, 'resize');
        if (this.route) {
            this.map.fitBounds(this.route.getBounds());
        }
    }
}

PMAMap.prototype.export = function () {
    var _data = this.data.route;

    //
    // escape all '\' in side our path so we saved it escaped
    //
    _data.path = window.btoa(google.maps.geometry.encoding.encodePath(this.route.getPath()));

    return _data;
}

//
// google map polyline extension method to get its bounds
//
PMAMap.prototype.initGoogleMapPrototypes = function () {
    if (!google.maps.Polyline.prototype.getBounds) {
        google.maps.Polyline.prototype.getBounds = function () {
            var bounds = new google.maps.LatLngBounds();
            var points = this.getPath().getArray();
            for (var n = 0; n < points.length; n++) {
                bounds.extend(points[n]);
            }
            return bounds;
        }
    }
}

//
// string prototypes
//
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
}

//
// auxiliar functions
//
function simplifyPath(points, toleranceAngle) {
    // return points if length <= 3
    if (points.length <= 3) {
        return points;
    }

    //
    // setting a default value for tolerance if there isn't one
    //
    toleranceAngle = toleranceAngle || 165;
    var cosTolerance = Math.cos(toleranceAngle * (Math.PI / 180));

    for (var i = 0; i < points.length - 2;) {
        //
        // getting our current points
        //
        var pointA = points[i];
        var pointB = points[i + 1];
        var pointC = points[i + 2];

        //
        // calculating distances between all the points
        //
        var _AB = google.maps.geometry.spherical.computeDistanceBetween(pointA, pointB);
        var _BC = google.maps.geometry.spherical.computeDistanceBetween(pointB, pointC);
        var _AC = google.maps.geometry.spherical.computeDistanceBetween(pointA, pointC);

        //
        // getting the cos of our angle _AB  ^ _BC
        //
        var cosX = (Math.pow(_AB, 2) + Math.pow(_BC, 2) - Math.pow(_AC, 2)) / (2 * _AB * _BC);

        //
        //  if the cosX is between cos(180) and the cos(toleranceAngle) we should remove it!
        // 
        if (cosX < cosTolerance) {
            points.splice(i + 1, 1);
            //
            // we need to iterate the whole thing again
            //
            i = 0;
            continue;
        }

        i++;
    }

    return points;
}
