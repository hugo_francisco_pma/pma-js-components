var PMAOdoUtils = {
    validationTypes:
    {
        SUBMIT: "SUBMIT"
    },
    constants: {
        STARTING_ODOMETER: "Starting Odometer is mandatory.",
        ENDING_ODOMETER: "Ending Odometer is mandatory.",
        STARTING_ENDING: "Starting and Ending Odometers must be filed.",
        STARTING_GREATER: "Starting Odometer cant be greater than Ending Odometer.",
        NEGATIVE_TOTAL: "Total shouldn't have negative value.",
        OVER_VALUES: "Values must be below 999,999.99"
    },
    defaultData: {
        rate: {
            odometerStart: 0,
            odometerEnd: 0,
            odometerDistance: 0
        }
    }
}

function PMAOdometer(options) {
    //
    // initialize all our UI components
    //
    this.odometerStart = $("#" + options.odometerStartId);
    this.odometerEnd = $("#" + options.odometerEndId);
    this.odometerDistance = $("#" + options.odometerDistanceId);
    this.odometerDistanceLabel = $("#" + options.odometerDistanceLabelId);
    this.odometerCheckbox = $("#" + options.odometerCheckboxId);
    this.labelRate = $("#" + options.labelRateId);
    this.alertDiv = $("#" + options.alertDivId);

    var validateAndCalculate = function() {
        if(this.validate()) {
            this.calculate();
        }
    }.bind(this);

    this.odometerCheckbox.off();
    this.odometerCheckbox.on('change', this.odometerCheckboxChanged.bind(this));

    this.odometerStart.off();
    this.odometerStart.on('keyup', validateAndCalculate);
    
    this.odometerEnd.off();
    this.odometerEnd.on('keyup', validateAndCalculate);
    onkeyup="pmaModal.calculate(); return false;"

    //
    // clear UI values
    //
    this.resetInterface();

    //
    // get our unit label using our option's unit system
    //
    var unitLabel = PMAMapUtils.constants.UNITS[options.unitSystem];
    //
    // change our labels to take our unit label into account
    //
    this.odometerDistanceLabel.html("Total (" + unitLabel + ")");
    this.labelRate.html("Rate = " + options.rate + " per " + unitLabel);
}

PMAOdometer.prototype.resetInterface = function() {
    //
    // clear ui values
    //
    this.odometerStart.val("");
    this.odometerEnd.val("");
    this.odometerDistance.val("");
}

PMAOdometer.prototype.odometerCheckboxChanged = function() {
    var isChecked = this.odometerCheckbox.is(":checked");

    this.odometerStart.prop('disabled', !isChecked);
    this.odometerEnd.prop('disabled', !isChecked);
    this.odometerDistance.prop('disabled', isChecked);
}

PMAOdometer.prototype.validate = function (type) {
    //
    // preparing our output
    //
    if (type != PMAMapUtils.validationTypes.SUBMIT) {
        this.odometerDistance.val("");
    }

    var errors = [];
    var isValid = true;

    if(!this.odometerStart.val()){
        errors.push(PMAOdoUtils.constants.STARTING_ODOMETER);
        this.odometerStart.addClass('has-error has-feedback');
    }

    if(!this.odometerEnd.val()){
        errors.push(PMAOdoUtils.constants.ENDING_ODOMETER);
        this.odometerEnd.addClass('has-error has-feedback');
    }

    if (!this.odometerEnd.val() && !this.odometerStart.val()) {
        errors.push(PMAOdoUtils.constants.STARTING_ENDING);
        this.odometerStart.addClass('has-error has-feedback');
        this.odometerEnd.addClass('has-error has-feedback');
    }

    if(this.odometerStart.val() > this.odometerEnd.val()){
        errors.push(PMAOdoUtils.constants.STARTING_GREATER);
        this.odometerEnd.addClass('has-error has-feedback');
    }
    
    if ((this.odometerEnd.val() - this.odometerStart.val()) < 0) {
        errors.push(PMAOdoUtils.constants.NEGATIVE_TOTAL);
        this.odometerStart.addClass('has-error has-feedback');
        this.odometerEnd.addClass('has-error has-feedback');
    }

    if ((this.odometerEnd.val() || this.odometerStart.val()) > 999999.99) {
        errors.push(PMAOdoUtils.constants.OVER_VALUES);
    }

    //
    // not valid? set our errors
    //
    isValid = errors.length == 0;
    if (!isValid) {
        isValid = false;

        var stringErrors = "";
        errors.forEach((element) => {
            stringErrors += element;
            stringErrors += "<br>";
        });

        //alert(stringErrors);
        this.alertDiv.html(stringErrors);
    }else{
        this.alertDiv.html("");
    }

    return isValid;
}

PMAOdometer.prototype.calculate = function () {
    this.odometerDistance.val(this.odometerEnd.val() - this.odometerStart.val());
}

PMAOdometer.prototype.fill = function (data) {
    // Fiil data
    this.odometerStart.val(data.startingodometer);
    this.odometerEnd.val(data.odometerEnd);
    this.odometerDistance.val(data.odometerDistance);
}

PMAOdometer.prototype.export = function () {
    return {
        odometerStart: this.odometerStart.val(),
        odometerEnd: this.odometerEnd.val(),
        odometerDistance: this.odometerDistance.val()
    };
}